const findStock = () => {
    // implementar aqui a chamada ao serviço de pesquisa de ação
}

const getDetails = () => {
    // implementar aqui a chamada ao serviço de busca de métricas

    let response = null; 

    let result = response.data
    return {
        yearHigherValue: result.metric['52WeekHigh'],
        yearLowerValue: result.metric['52WeekLow'],
        lowerValueDate: new Date(result.metric['52WeekLowDate'])
    }
}

const stocksService = {findStock, getDetails}

export default stocksService