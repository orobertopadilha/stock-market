import { AppBar, Toolbar, Typography } from "@mui/material"

const AppToolbar = () => {
    return (
        <AppBar position="sticky">
            <Toolbar>
                <Typography variant="h6">Stock Market</Typography>
            </Toolbar>
        </AppBar>
    )
}

export default AppToolbar