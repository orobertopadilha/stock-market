import { Button, Grid, Typography } from "@mui/material"
import { Box } from "@mui/system"
import moment from "moment"
import { useState } from "react"
import { useHistory } from 'react-router-dom'

const StocksDetails = () => {
    const [details, setDetails] = useState(null)

    const history = useHistory()

    return (
        <>
            {
                details &&
                <Box marginTop={3}>
                    <Typography variant="h5">
                        Detalhes para a ação {stockSymbol}
                    </Typography>
                    <Typography>
                        Considerando os últimos 12 meses
                    </Typography>
                    <Grid container spacing={1} marginTop={1}>
                        <Grid item xs={2}>
                            <Typography variant="h6">{details.yearHigherValue}</Typography>
                            <Typography variant="caption">Maior valor</Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="h6">{details.yearLowerValue}</Typography>
                            <Typography variant="caption">Menor valor</Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="h6">{moment(details.lowerValueDate).format('DD/MM/YYYY')}</Typography>
                            <Typography variant="caption">Data do menor valor</Typography>
                        </Grid>
                    </Grid>
                    <Button variant="contained" style={{marginTop: '10px'}} onClick={() => history.goBack()}>Voltar</Button>
                </Box>
            }
        </>
    )
}

export default StocksDetails