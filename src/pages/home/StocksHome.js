import { useState } from "react"

import StockSearch from "./StockSearch"
import StocksList from "./StocksList"

const StocksHome = () => {
    const [stocks, setStocks] = useState([])

    return (
        <>
            <StockSearch onSearchResult={setStocks} />
            <StocksList stocks={stocks} />
        </>
    )
}

export default StocksHome