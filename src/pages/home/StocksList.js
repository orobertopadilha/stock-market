import { Typography } from "@mui/material"
import StockItem from "./StockItem"

const StocksList = ({ stocks }) => {

    return (
        <div style={{ marginTop: '20px' }}>
            {
                stocks.length > 0 ?
                    stocks.map(stock =>
                        <StockItem key={stock.symbol} 
                            type={stock.type} 
                            symbol={stock.symbol} 
                            description={stock.description} />
                    )
                    :
                    <Typography textAlign="center">Nenhum resultado encontrado</Typography>
            }
        </div>
    )

}

export default StocksList