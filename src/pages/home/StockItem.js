import { Paper, Stack, Typography } from "@mui/material"
import { Box } from "@mui/system"
import { useHistory } from "react-router"

const StockItem = ({ description, type, symbol }) => {

    const history = useHistory()

    const openDetails = () => {
        history.push(`/stock/${symbol}`)
    }

    return (
        <Box padding={2} borderRadius={2} component={Paper} marginTop={3} onClick={openDetails} style={{cursor: 'pointer'}}>
            <Stack direction="row" alignItems="center">
                <Stack flex={1}>
                    <Typography variant="h6">{description}</Typography>
                    <Typography>{type}</Typography>
                </Stack>
                <Typography>{symbol}</Typography>
            </Stack>
        </Box>
    )
}

export default StockItem