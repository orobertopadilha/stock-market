import { Button, Grid, TextField } from "@mui/material"
import { useState } from "react"
import stocksService from "../../services/StocksService"

const StockSearch = ({ onSearchResult }) => {
    const [searchText, setSearchText] = useState('')

    const findStock = async () => {
        let result = await stocksService.findStock(searchText)
        onSearchResult(result)
    }

    return (
        <Grid container spacing={1} marginTop={2}>
            <Grid item xs={11}>
                <TextField
                    label="Pesquisar ação"
                    variant="outlined"
                    fullWidth
                    size="small"
                    value={searchText}
                    onChange={e => setSearchText(e.target.value)}
                />
            </Grid>
            <Grid item xs={1}>
                <Button variant="contained" onClick={findStock}>Buscar</Button>
            </Grid>
        </Grid>
    )

}

export default StockSearch