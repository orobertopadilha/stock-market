import AppToolbar from "./components/AppToolbar";

const App = () => {

    return (
        <>
            <AppToolbar />
        </>
    )

}

export default App;